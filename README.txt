# Zależności

  * Biblioteka ITK w wersji 4.7.x http://www.itk.org/
  * cmake >=2.4
  * GNU make

# Kompilacja

Będąc w katalogu głównym projektu uruchom:

    cd build
    cmake ../src
    make

# Uruchomienie

W katalogu build:

    ./mri plik.conf
    
# Przykładowy plik konfiguracyjny

Komentarze i spacje są nieobsługiwane.
Aby przeprowadzić obliczenia dla nieznanego szumu, należy usunąć parametr `input_filename_SNR`:

    ex_filter_type=2
    ex_window_size=5
    ex_iterations=10
    lpf_f=3.4
    lpf_f_SNR=1.2
    lpf_f_Rice=5.4
    input_filename=MR_noisy.csv
    input_filename_SNR=MR_SNR.csv
    output_filename_Gaussian=MR_Gaussian_Map.csv
    output_filename_Rician=MR_Rician_Map.csv