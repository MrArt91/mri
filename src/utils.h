#pragma once

#include "itkImage.h"
#include "types.h"

ImageType::Pointer loadImage(const char* fileName);
void saveImage(const char* fileName, ImageType::Pointer image);
