#pragma once

typedef itk::Image<double, 2> ImageType;
typedef double (*FilterMapFunction)(double pixelValue);
