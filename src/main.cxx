#include "utils.h"
#include "types.h"
#include "filters.h"
#include "itkImage.h"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

ImageType::Pointer gaussianPipeline(ImageType::Pointer _image);
ImageType::Pointer ricePipeline(ImageType::Pointer _image);
bool parseConfig(const char* filename);

unsigned short ex_filter_type = -1; // 1 - local mean,
// 2 - expectation-maximization (EM).
unsigned short ex_window_size = -1; // window size for E{X},
unsigned short ex_iterations = -1; // number of iterations of the EM algorithm (used only by EM),
float lpf_f = -1; // sigma for LPF filter,
float lpf_f_SNR = -1; // sigma for LPF filter; used to smooth sigma(x) in SNR,
float lpf_f_Rice = -1; // sigma for LPF filter; used to smooth Rician corrected noise map,
char input_filename[256] = ""; // Noisy MR image,
char input_filename_SNR[256] = "";
char output_filename_Gaussian[256] = ""; // estimated noise map for Gaussian case,
char output_filename_Rician[256] = ""; // estimated noise map for Rician case,

int main(int argc, char **argv)
{
    if (argc < 2) {
        fprintf(stderr, "Need configuration file as parameter!");
        return 1;
    }

    parseConfig(argv[1]);

    ImageType::Pointer input = loadImage(input_filename);

    if (input == (void *)NULL) {
        fprintf(stderr, "Failed to load input file: %s", input_filename);
        return 1;
    }

    ImageType::Pointer gImage = gaussianPipeline(input);
    ImageType::Pointer rImage = ricePipeline(input);

    saveImage(output_filename_Gaussian, gImage);
    saveImage(output_filename_Rician, rImage);

    return 0;
}

ImageType::Pointer gaussianPipeline(ImageType::Pointer _image)
{
    ImageType::Pointer image = Filter::duplicate(_image);

    ImageType::Pointer meanImage = Filter::mean(image, ex_window_size);
    image = Filter::subtract(image, meanImage);
    image = Filter::abs(image);
    image = Filter::log(image);
    image = Filter::lowpass(image, lpf_f);
    image = Filter::add(image, 0.288607832450766);
    image = Filter::exp(image);
    image = Filter::multiply(image, 1.41421356237309);

    return image;
}

double lessThan7(double val)
{
    return val <= 7 ? 1 : 0;
}

ImageType::Pointer correct(ImageType::Pointer image)
{
    double c[9];
    c[0] = -0.289549906258443;
    c[1] = -0.0388922575606330;
    c[2] = 0.409867108141953;
    c[3] = -0.355237628488567;
    c[4] = 0.149328280945610;
    c[5] = -0.0357861117942093;
    c[6] = 0.00497952893859122;
    c[7] = -0.000374756374477592;
    c[8] = 0.0000118020229140092;

    ImageType::Pointer o = Filter::multiply(image, 0);

    for (int i = 0; i < 9; ++i)
    {
        o = Filter::add(o, Filter::multiply(Filter::powImage(image, i), c[i]));
    }

    ImageType::Pointer lt7 = Filter::mapPixel(image, lessThan7);
    o = Filter::multiply(o, lt7);

    return o;
}

double doNotAllowZero(double val)
{
    return val == 0 ? 0.001 : val;
}

ImageType::Pointer ricePipeline(ImageType::Pointer _image)
{
    ImageType::Pointer image = Filter::duplicate(_image);
    ImageType::Pointer toSubtract;

    ImageType::Pointer emMean;
    ImageType::Pointer emNoise;

    Filter::em(image, 10, &emMean, &emNoise);

    //emMean = loadImage("emMean.csv");
    //emNoise = loadImage("emNoise.csv");
    
    switch (ex_filter_type) {
        // local mean
        case 1:
            toSubtract = Filter::mean(image, ex_window_size);
            break;
        // EM
        case 2:
            toSubtract = emMean;
            break;
        default:
            toSubtract = Filter::multiply(image, 0);
    }

    image = Filter::subtract(image, toSubtract);
    image = Filter::abs(image);
    image = Filter::mapPixel(image, doNotAllowZero);
    image = Filter::log(image);
    image = Filter::lowpass(image, lpf_f);

    ImageType::Pointer snr;

    // znane SNR
    if (input_filename_SNR[0] != 0) {
        snr = loadImage(input_filename_SNR);

        if (snr == (void *)NULL) {
            fprintf(stderr, "Failed to load input file: %s", input_filename);
        }
    }
    
    if (snr == (void *)NULL) {
        // nieznane SNR
        snr = Filter::divide(emMean, Filter::lowpass(emNoise, lpf_f_SNR));
    }

    image = Filter::subtract(image, correct(snr));

    image = Filter::lowpass(image, lpf_f_Rice);

    image = Filter::add(image, 0.288607832450766);
    image = Filter::exp(image);
    image = Filter::multiply(image, 1.41421356237309);

    return image;
}

void store_line(string &key, std::string &value)
{
    if (!key.compare("ex_filter_type")) {
        ex_filter_type = atoi(value.c_str());
    } else if (!key.compare("ex_window_size")) {
        ex_window_size = atoi(value.c_str());
    } else if (!key.compare("ex_iterations")) {
        ex_iterations = atoi(value.c_str());
    } else if (!key.compare("lpf_f")) {
        lpf_f = atof(value.c_str());
    } else if (!key.compare("lpf_f_SNR")) {
        lpf_f_SNR = atof(value.c_str());
    } else if (!key.compare("lpf_f_Rice")) {
        lpf_f_Rice = atof(value.c_str());
    } else if (!key.compare("input_filename")) {
        strcpy(input_filename, value.c_str());
    } else if (!key.compare("input_filename_SNR")) {
        strcpy(input_filename_SNR, value.c_str());
    } else if (!key.compare("output_filename_Gaussian")) {
        strcpy(output_filename_Gaussian, value.c_str());
    } else if (!key.compare("output_filename_Rician")) {
        strcpy(output_filename_Rician, value.c_str());
    }
}

bool parseConfig(const char* filename)
{
    std::ifstream is_file(filename);
    std::string line;

    while( std::getline(is_file, line) )
    {
        std::istringstream is_line(line);
        std::string key;

        if( std::getline(is_line, key, '=') )
        {
            std::string value;
            if( std::getline(is_line, value) ) 
                store_line(key, value);
        }
    }

    return true;
}
