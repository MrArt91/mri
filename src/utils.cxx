#include "utils.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

ImageType::Pointer loadImage(const char* filename) {
    std::vector< std::vector<double> > imgVector;
    std::string cell;

    std::string line;
    std::ifstream myfile(filename);
    if (myfile.is_open() == false) {
        return NULL;
    } else {
        while (getline(myfile,line)) {
            std::stringstream lineStream(line);
            std::vector<double> lineVector;

            while (std::getline(lineStream, cell, ',')) {
                lineVector.push_back(atof(cell.c_str()));
            }

            imgVector.push_back(lineVector);
        }
    }

    myfile.close();

    ImageType::IndexType start;
    ImageType::SizeType size;
    ImageType::RegionType region;
    ImageType::Pointer image = ImageType::New();

    start[0] = 0;
    start[1] = 0;

    size[0] = imgVector[0].size();
    size[1] = imgVector.size();

    region.SetSize(size);
    region.SetIndex(start);

    image->SetRegions(region);
    image->Allocate();

    for (int y = 0; y < size[1]; ++y) {
        for (int x = 0; x < size[0]; ++x) {
            ImageType::IndexType pixelIndex = {{x, y}};
            image->SetPixel(pixelIndex, imgVector[y][x]);
        }
    }

    return image;
}

void saveImage(const char* fileName, ImageType::Pointer image)
{
    std::ofstream myfile (fileName);
    ImageType::SizeType size = image->GetLargestPossibleRegion().GetSize();
    double pixel;

    if (myfile.is_open())
    {
        for (int y = 0; y < size[1]; ++y)
        {
            for (int x = 0; x < size[0]; ++x)
            {
                ImageType::IndexType pixelIndex = {{x, y}};
                pixel = image->GetPixel(pixelIndex);
                
                myfile << pixel;
                if (x != size[0] - 1) myfile << ",";
            }
            
            myfile << std::endl;
        }
    }

    myfile.close();
}
