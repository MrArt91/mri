#pragma once

#include "itkImage.h"
#include "types.h"

namespace Filter
{
    ImageType::Pointer mean(ImageType::Pointer image, unsigned short windowSize);
    ImageType::Pointer subtract(ImageType::Pointer image1, ImageType::Pointer image2);
    ImageType::Pointer subtract(double val, ImageType::Pointer image);
    ImageType::Pointer abs(ImageType::Pointer image);
    ImageType::Pointer log(ImageType::Pointer image);
    ImageType::Pointer lowpass(ImageType::Pointer image, double sigma);
    ImageType::Pointer add(ImageType::Pointer image, double val);
    ImageType::Pointer add(ImageType::Pointer image1, ImageType::Pointer image2);
    ImageType::Pointer multiply(ImageType::Pointer image, double val);
    ImageType::Pointer multiply(ImageType::Pointer image1, ImageType::Pointer image2);
    ImageType::Pointer divide(ImageType::Pointer image1, ImageType::Pointer image2);
    ImageType::Pointer divide(double val, ImageType::Pointer image);
    ImageType::Pointer powImage(ImageType::Pointer image, double val);
    ImageType::Pointer exp(ImageType::Pointer image);
    ImageType::Pointer sqrt(ImageType::Pointer image);
    ImageType::Pointer duplicate(ImageType::Pointer image);
    void em(ImageType::Pointer image, unsigned short n, ImageType::Pointer* pSignal, ImageType::Pointer* pSigma);
    ImageType::Pointer besseli(unsigned short k, ImageType::Pointer _image);
    ImageType::Pointer max(ImageType::Pointer image, double val);
    ImageType::Pointer mapPixel(ImageType::Pointer _image, FilterMapFunction func);
    ImageType::Pointer polynomial(ImageType::Pointer image, const double *coeff, unsigned short n);
    ImageType::Pointer approxI1_I0(ImageType::Pointer z);
}
