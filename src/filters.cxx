#include "filters.h"
#include "itkMeanImageFilter.h"
#include "itkSubtractImageFilter.h"
#include "itkAbsImageFilter.h"
#include "itkLogImageFilter.h"
#include "itkDiscreteGaussianImageFilter.h"
#include "itkAddImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkDivideImageFilter.h"
#include "itkPowImageFilter.h"
#include "itkExpImageFilter.h"
#include "itkSqrtImageFilter.h"
#include "itkImageDuplicator.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkMaximumImageFilter.h"

#include "itkWrapPadImageFilter.h"
#include "itkForwardFFTImageFilter.h"
#include "itkFFTShiftImageFilter.h"
#include "itkInverseFFTImageFilter.h"
#include "itkImageRegionIteratorWithIndex.h"

#include "math.h"

ImageType::Pointer Filter::mean(ImageType::Pointer image, unsigned short windowSize)
{
    typedef itk::MeanImageFilter<ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    ImageType::SizeType indexRadius;
    indexRadius[0] = windowSize / 2; // radius along x
    indexRadius[1] = windowSize / 2; // radius along y
    filter->SetRadius(indexRadius);
    filter->SetInput(image);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::subtract(ImageType::Pointer image1, ImageType::Pointer image2)
{
    typedef itk::SubtractImageFilter<ImageType, ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetInput1(image1);
    filter->SetInput2(image2);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::subtract(double val, ImageType::Pointer image)
{
    typedef itk::SubtractImageFilter<ImageType, ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetConstant1(val);
    filter->SetInput2(image);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::abs(ImageType::Pointer image)
{
    typedef itk::AbsImageFilter<ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetInput(image);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::log(ImageType::Pointer image)
{
    typedef itk::LogImageFilter<ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetInput(image);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer gaussian(ImageType::SizeType size, double sigma)
{
    ImageType::IndexType start;
    ImageType::RegionType region;
    ImageType::Pointer image = ImageType::New();

    start[0] = 0;
    start[1] = 0;

    region.SetSize(size);
    region.SetIndex(start);

    image->SetRegions(region);
    image->Allocate();
    double h1 = (double)(size[0] - 1) / 2;
    double h2 = (double)(size[1] - 1) / 2;

    double val, x, y;
    double max = 0;

    itk::ImageRegionIteratorWithIndex<ImageType> it(image, region);

    while (!it.IsAtEnd())
    {
        x = it.GetIndex()[0];
        y = it.GetIndex()[1];
        
        val = exp(-(pow(h1 - x, 2) + pow(h2 - y, 2)) / (2 * sigma * sigma));
        it.Set(val);

        max = val > max ? val : max;
        ++it;
    }

    
    if (max != 0)
    {
        image = Filter::multiply(image, 1/max);
    }

    return image;
}

ImageType::Pointer Filter::lowpass(ImageType::Pointer image, double sigmaValue)
{
    typedef itk::ForwardFFTImageFilter< ImageType > ForwardFFTFilterType;
    typedef ForwardFFTFilterType::OutputImageType ComplexImageType;
    typedef itk::MultiplyImageFilter< ComplexImageType, ImageType, ComplexImageType > MultiplyFilterType;
    typedef itk::FFTShiftImageFilter< ImageType, ImageType > FFTShiftFilterType;
    typedef itk::InverseFFTImageFilter< ComplexImageType, ImageType > InverseFilterType;

    ForwardFFTFilterType::Pointer forwardFFTFilter = ForwardFFTFilterType::New();
    forwardFFTFilter->SetInput(image);

    try
    {
        forwardFFTFilter->UpdateOutputInformation();
    }
    catch( itk::ExceptionObject & error )
    {
        std::cerr << "Error: " << error << std::endl;
    }

    ImageType::Pointer gaussianImage = gaussian(image->GetLargestPossibleRegion().GetSize(), sigmaValue);

    FFTShiftFilterType::Pointer fftShiftFilter = FFTShiftFilterType::New();
    fftShiftFilter->SetInput(gaussianImage);

    MultiplyFilterType::Pointer multiplyFilter = MultiplyFilterType::New();
    multiplyFilter->SetInput1( forwardFFTFilter->GetOutput() );
    multiplyFilter->SetInput2( fftShiftFilter->GetOutput() );

    InverseFilterType::Pointer inverseFFTFilter = InverseFilterType::New();
    inverseFFTFilter->SetInput( multiplyFilter->GetOutput() );

    inverseFFTFilter->Update();

    return inverseFFTFilter->GetOutput();
}

ImageType::Pointer Filter::add(ImageType::Pointer image1, ImageType::Pointer image2)
{
    typedef itk::AddImageFilter<ImageType, ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetInput1(image1);
    filter->SetInput2(image2);
    filter->Update();

    return filter->GetOutput();
}


ImageType::Pointer Filter::add(ImageType::Pointer image, double val)
{
    typedef itk::AddImageFilter<ImageType, ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetInput1(image);
    filter->SetConstant2(val);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::multiply(ImageType::Pointer image, double val)
{
    typedef itk::MultiplyImageFilter<ImageType, ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetInput1(image);
    filter->SetConstant2(val);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::multiply(ImageType::Pointer image1, ImageType::Pointer image2)
{
    typedef itk::MultiplyImageFilter<ImageType, ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetInput1(image1);
    filter->SetInput2(image2);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::divide(ImageType::Pointer image1, ImageType::Pointer image2)
{
    typedef itk::DivideImageFilter<ImageType, ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetInput1(image1);
    filter->SetInput2(image2);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::divide(double val, ImageType::Pointer image)
{
    typedef itk::DivideImageFilter<ImageType, ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetConstant1(val);
    filter->SetInput2(image);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::powImage(ImageType::Pointer image, double val)
{
    typedef itk::PowImageFilter<ImageType, ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetInput1(image);
    filter->SetConstant2(val);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::exp(ImageType::Pointer image)
{
    typedef itk::ExpImageFilter<ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetInput(image);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::sqrt(ImageType::Pointer image)
{
    typedef itk::SqrtImageFilter<ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetInput(image);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::duplicate(ImageType::Pointer image)
{
    typedef itk::ImageDuplicator< ImageType > DuplicatorType;

    DuplicatorType::Pointer duplicator = DuplicatorType::New();
    duplicator->SetInputImage(image);
    duplicator->Update();

    return duplicator->GetOutput();
}

ImageType::Pointer Filter::besseli(unsigned short k, ImageType::Pointer _image)
{
    ImageType::Pointer image = Filter::duplicate(_image);

    typedef itk::GaussianOperator<float, 1> GaussianOperatorType;
    GaussianOperatorType gaussianOperator;
    itk::ImageRegionIteratorWithIndex<ImageType> it(image, image->GetLargestPossibleRegion());

    while (!it.IsAtEnd())
    {
        switch (k)
        {
            case 0: 
                it.Set(gaussianOperator.ModifiedBesselI0(it.Value()));
                break;
            case 1:
                it.Set(gaussianOperator.ModifiedBesselI1(it.Value()));
                break;
            default:
                it.Set(gaussianOperator.ModifiedBesselI(k, it.Value()));
        }

        ++it;
    }

    return image;
}

double max(ImageType::Pointer image)
{
    typedef itk::MinimumMaximumImageCalculator <ImageType> ImageCalculatorFilterType;

    ImageCalculatorFilterType::Pointer imageCalculatorFilter = ImageCalculatorFilterType::New ();
    imageCalculatorFilter->SetImage(image);
    imageCalculatorFilter->ComputeMaximum();

    return imageCalculatorFilter->GetMaximum();
}

ImageType::Pointer Filter::max(ImageType::Pointer image, double val)
{
    typedef itk::MaximumImageFilter<ImageType, ImageType, ImageType> FilterType;
    FilterType::Pointer filter = FilterType::New();

    filter->SetInput(0, image);
    filter->SetConstant(val);
    filter->Update();

    return filter->GetOutput();
}

ImageType::Pointer Filter::polynomial(ImageType::Pointer image, const double *coeff, unsigned short n)
{
    ImageType::Pointer o = Filter::multiply(image, 0);

    for (int i = 0; i < n; ++i)
    {
        o = Filter::add(o, Filter::multiply(Filter::powImage(image, i), coeff[i]));
    }

    return o;
}

double negateInf(double val)
{
    if (val == INFINITY) return -INFINITY;

    return val;
}

double besseli0(double x) {
    double ax = fabs(x);
    double ans, y;
 
    if (ax < 3.75) {
        y = x / 3.75;
        y = y * y;
        ans = 1.0+y*(3.5156229+y*(3.0899424+y*(1.2067492+y*(0.2659732+y*(0.360768e-1+y*0.45813e-2)))));
    } else {
        y = 3.75 / ax;
        ans = (exp(ax)/sqrt(ax))*(0.39894228+y*(0.1328592e-1+y*(0.225319e-2+y*(-0.157565e-2+y*(0.916281e-2+y*(-0.2057706e-1+y*(0.2635537e-1+y*(-0.1647633e-1+y*0.392377e-2))))))));
    }
    return ans;
}
 
double besseli1(double x) {
    double ax = fabs(x);
    double ans, y;
 
    if (ax < 3.75) {
        y = x/3.75;
        y = y*y;
        ans=ax*(0.5+y*(0.87890594+y*(0.51498869+y*(0.15084934+y*(0.2658733e-1+y*(0.301532e-2+y*0.32411e-3))))));
    } else {
        y = 3.75 / ax;
        ans = 0.2282967e-1+y*(-0.2895312e-1+y*(0.1787654e-1-y*0.420059e-2));
        ans = 0.39894228+y*(-0.3988024e-1+y*(-0.362018e-2+y*(0.163801e-2+y*(-0.1031555e-1+y*ans))));
        ans *= (exp(ax)/sqrt(ax));
    }
    return x < 0.0 ? -ans : ans;
}

ImageType::Pointer Filter::approxI1_I0(ImageType::Pointer z)
{
    typedef itk::GaussianOperator<float, 1> GaussianOperatorType;
    GaussianOperatorType go;

    ImageType::Pointer z8 = multiply(z, 8);

    double mnCoeff[] = { 1, -3, -7.5, -52.5 };
    double mdCoeff[] = { 1, 1, 4.5, 37.5 };

    ImageType::Pointer mn = Filter::multiply(z, 0);
    ImageType::Pointer md = Filter::multiply(z, 0);

    for (int i = 0; i < 4; ++i)
    {
        mn = Filter::add(mn, divide(mnCoeff[i], Filter::powImage(z8, i)));
        md = Filter::add(md, divide(mdCoeff[i], Filter::powImage(z8, i)));
    }

    //mn = mapPixel(mn, negateInf);
    //md = mapPixel(md, negateInf);

    ImageType::Pointer m = divide(mn, md);

    ImageType::SizeType size = z->GetLargestPossibleRegion().GetSize();

    for (int x = 0; x < size[0]; ++x)
    {
        for (int y = 0; y < size[0]; ++y)
        {
            ImageType::IndexType pixelIndex = {{x, y}};
            double zVal = z->GetPixel(pixelIndex);

            if (zVal == 0)
            {
                m->SetPixel(pixelIndex, 0);
            }
            else if (zVal < 1.5)
            {
                m->SetPixel(pixelIndex, go.ModifiedBesselI1(zVal) / go.ModifiedBesselI0(zVal));
                //m->SetPixel(pixelIndex, besseli1(zVal) / besseli0(zVal));
            }
        }
    }

    return m;
}

void Filter::em(ImageType::Pointer image, unsigned short n, ImageType::Pointer* pSignal, ImageType::Pointer* pSigma)
{
    ImageType::Pointer image2 = multiply(image, image);
    ImageType::Pointer image4 = multiply(image2, image2);

    ImageType::Pointer alfa2 = sqrt(max(subtract(multiply(powImage(mean(image2, 3), 2), 2), mean(image4, 3)), 0));
    ImageType::Pointer alfa = sqrt(alfa2);

    ImageType::Pointer sigma2 = multiply(max(subtract(mean(image2, 3), alfa2), 0.01), 0.5);

    ImageType::Pointer besseliInput;

    for (int i = 0; i < n; ++i)
    {
        besseliInput = divide(multiply(alfa, image), sigma2);
        alfa = max(mean(multiply(approxI1_I0(besseliInput), image), 3), 0);
        alfa2 = powImage(alfa, 2);
        sigma2 = max(subtract(multiply(mean(image2, 3), 0.5), multiply(alfa2, 0.5)), 0.01);

        /*
        ImageType::Pointer newAlfa = max(mean(multiply(approxI1_I0(besseliInput), image), 3), 0);
        ImageType::Pointer newAlfa2 = powImage(alfa, 2);
        sigma2 = max(subtract(multiply(mean(image2, 3), 0.5), multiply(alfa2, 0.5)), 0.01);
        alfa = newAlfa;
        alfa2 = newAlfa2;
        */
    }

    *pSignal = alfa;
    *pSigma = sqrt(sigma2);
}

ImageType::Pointer Filter::mapPixel(ImageType::Pointer _image, FilterMapFunction func)
{
    ImageType::Pointer image = Filter::duplicate(_image);

    itk::ImageRegionIteratorWithIndex<ImageType> it(image, image->GetLargestPossibleRegion());

    while (!it.IsAtEnd())
    {
        it.Set(func(it.Value()));
        ++it;
    }

    return image;
}
